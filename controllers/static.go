package controllers

import "lenslocked.com/views"

func NewStatic() *Static {
	return &Static{
		Hone:    views.NewView("bootstrap", "static/home"),
		Contact: views.NewView("bootstrap", "static/contact"),
		Faq:     views.NewView("bootstrap", "static/faq"),
	}
}

type Static struct {
	Hone    *views.View
	Contact *views.View
	Faq     *views.View
}
