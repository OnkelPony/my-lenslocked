package main

import (
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"lenslocked.com/models"
)

const (
	host   = "localhost"
	port   = 5432
	user   = "gio"
	dbname = "lenslocked_dev"
)

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"dbname=%s sslmode=disable",
		host, port, user, dbname)

	us, err := models.NewUserService(psqlInfo)
	if err != nil {
		panic(err)
	}

	defer us.Close()
	us.DestructiveReset()

	// // Create user
	// user := models.User{
	// 	Name:  "Aletta Ocean",
	// 	Email: "aletta@ocean.porn",
	// }
	// if err := us.Create(&user); err != nil {
	// 	panic(err)
	// }
	//
	// user.Name = "lady Aletta von Ocean"
	// if err = us.Update(&user); err != nil {
	// 	panic(err)
	// }
	//
	// foundUser, err := us.ByEmail("aletta@ocean.porn")
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println(foundUser)
	//
	// // Delete user
	// if err = us.Delete(foundUser.ID); err != nil {
	// 	panic(err)
	// }
	//
	// // Verify the user is deleted
	// _, err = us.ByID(foundUser.ID)
	// if err != models.ErrNotFound {
	// 	panic("user was not deleted")
	// }
}
